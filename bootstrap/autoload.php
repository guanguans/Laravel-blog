<?php

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so we do not have to manually load any of
| our application's PHP classes. It just feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';
// 自定义全局辅助函数
require __DIR__ . '/../app/vendor/function.php';
// 自定义全局辅助函数
require __DIR__ . '/../app/vendor/Tree.class.php';
// 上传类
require __DIR__ . '/../app/vendor/UploadFile.class.php';
