<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin', 'Admin\IndexController@index');
Route::get('login', 'Admin\LoginController@login');
Route::post('doLogin', 'Admin\LoginController@doLogin');
Route::get('logout', 'Admin\LoginController@logout');


Route::group(['prefix' => 'admin'], function () {
    // 登录
    Route::get('', 'Admin\IndexController@index');
    Route::get('login', 'Admin\LoginController@login');
    Route::post('doLogin', 'Admin\LoginController@doLogin');
    Route::get('logout', 'Admin\LoginController@logout');
    // 用户
    Route::get('userIndex', 'Admin\UserController@index');
    Route::get('userCreate', 'Admin\UserController@create');
    Route::post('userStore', 'Admin\UserController@store');
    // 分类
    Route::get('categoryIndex', 'Admin\CategoryController@index');
    Route::get('categoryCreate/{parent?}', 'Admin\CategoryController@create');
    Route::post('categoryStore', 'Admin\CategoryController@store');
    Route::get('categoryShow/{term_id}', 'Admin\CategoryController@show');
    Route::post('categoryUpdate/{term_id}', 'Admin\CategoryController@update');
    Route::get('categoryDestroy/{term_id}', 'Admin\CategoryController@destroy');
    // 文章
    Route::get('postIndex', 'Admin\PostController@index');
    Route::get('postCreate', 'Admin\PostController@create');
    Route::post('postStore', 'Admin\PostController@store');
    Route::get('postEdit/{post_id}', 'Admin\PostController@edit');
    Route::post('postUpdate/{post_id}', 'Admin\PostController@update');
    Route::get('postDestroy/{post_id}', 'Admin\PostController@destroy');
    Route::post('postUpload', 'Admin\PostController@upload');
    Route::get('postStatus/{post_id}/{status}', 'Admin\PostController@status');
    Route::get('postIstop/{post_id}/{istop}', 'Admin\PostController@istop');
    Route::get('postRecommended/{post_id}/{recommended}', 'Admin\PostController@recommended');
    Route::post('postBatchStatus/{post_id}/{status}', 'Admin\PostController@batch_status');
    Route::post('postBatchIstop/{post_id}/{istop}', 'Admin\PostController@batch_istop');
    Route::post('postBatchRecommended/{post_id}/{recommended}', 'Admin\PostController@batch_recommended');
    // 回收站
    Route::get('recyclePostIndex', 'Admin\RecycleController@postIndex');
    Route::get('recyclePostRestore/{post_id}', 'Admin\RecycleController@postRestore');
    Route::get('recyclePostDestroy/{post_id}', 'Admin\RecycleController@postDestroy');
    // 友情链接
    Route::get('linksIndex', 'Admin\LinksController@index');
    Route::get('linksCreate', 'Admin\LinksController@create');
    Route::post('linksStore', 'Admin\LinksController@store');
    Route::get('linksEdit/{link_id}', 'Admin\LinksController@edit');
    Route::post('linksUpdate/{link_id}', 'Admin\LinksController@update');
    Route::get('linksShow/{link_id}', 'Admin\linksController@show');
    Route::get('linksHide/{link_id}', 'Admin\LinksController@hide');
    Route::get('linksDestroy/{link_id}', 'Admin\LinksController@destroy');
    // 个人信息
    Route::get('personEditInfo/{user_id}', 'Admin\PersonController@editInfo');
    Route::post('personUpdateInfo/{user_id}', 'Admin\PersonController@updateInfo');
    Route::get('personEditPass/{user_id}', 'Admin\PersonController@editPass');
    Route::post('personUpdatePass/{user_id}', 'Admin\PersonController@updatePass');
});
