<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use DB;

class LoginController extends AdminBaseController
{

    /**
     * 登录页面
     */
    public function login()
    {
        return view('admin.login');
    }

    /**
     * 登录
     */
    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'user_login' => 'required|min:5|max:8',
            'user_pass' => 'required|min:5|max:10',
            'captcha' => 'required|captcha',

        ]);
        $user_login = $request->input('user_login');
        $user_pass = en_password($request->input('user_pass'));
        $res = DB::table('cmf_users')->where('user_login', $user_login)->where('user_pass', $user_pass)->first();
        if ($res) {
            DB::table('cmf_users')->where('user_login', $user_login)->where('user_pass', $user_pass)->update([
                'last_login_ip'=>get_client_ip(),
                'last_login_time'=>time(),
            ]);
            $request->session()->put('admin_id', $res->id);
            $request->session()->put('admin_login', $res->user_login);
            return redirect('/admin');
        } else {
            return back()->with('info', '用户名或密码错误');
        }
    }

    /**
     * 退出
     */
    public function logout(Request $request)
    {
        $request->session()->forget('admin_id');
        $request->session()->forget('admin_login');
        return redirect('admin/login');
    }


}
