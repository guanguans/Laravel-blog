<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Model\Admin\Post;
use DB;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Validator;
use App\Model\Admin\Category;
use App\Model\Admin\TermRelationships;

class RecycleController extends AdminBaseController
{
    /**
     * �б�
     */
    public function postIndex()
    {
        $tree = new \Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;��', '&nbsp;&nbsp;&nbsp;���� ', '&nbsp;&nbsp;&nbsp;���� ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $terms = Category::get()->toArray();

        $new_terms=array();
        foreach ($terms as $r) {
            $r['id']=$r['term_id'];
            $r['parentid']=$r['parent'];
            $r['selected']= (!empty($parentid) && $r['term_id']==$parentid)? "selected":"";
            $new_terms[] = $r;
        }
        $tree->init($new_terms);
        $tree_tpl="<option value='\$id' \$selected>\$spacer\$name</option>";
        $taxonomys=$tree->get_tree(0,$tree_tpl);
        $posts = Post::where('recommended', '!=', 3)
                ->where('post_status', '=', 3)
                ->paginate(15);
        return view('admin.recycle.index', ['posts'=>$posts, 'taxonomys'=>$taxonomys]);
    }

    /**
     * ��ԭ
     */
    public function postRestore(Request $request, $id)
    {
        $res = Post::where('id',  $id)->update(['post_status'=>0]);
        if ($res) {
            return redirect('admin/recyclePostIndex');
        } else {
            $request->session()->flash('error', '��ԭʧ�ܣ�');
            return redirect('admin/recyclePostIndex');
        }
    }

    /**
     * ɾ��
     */
    public function postDestroy(Request $request, $id)
    {
        $resa = Post::where('id',  $id)->delete();
        $resb = TermRelationships::where('object_id',  $id)->delete();
        if ($resa && $resb) {
            return redirect('admin/recyclePostIndex');
        } else {
           $request->session()->flash('error', 'ɾ��ʧ�ܣ�');
            return redirect('admin/recyclePostIndex');
        }
    }
}
