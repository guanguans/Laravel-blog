<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Model\Admin\Links;
use DB;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Validator;

class LinksController extends AdminBaseController
{
    /**
     * 列表
     */
    public function index()
    {
        $links = Links::get();
        return view('admin.links.index', ['links'=>$links]);
    }

    /**
     * 添加
     */
    public function create()
    {
        return view('admin.links.create');
    }

    /**
     * 保存
     */
    public function store(Request $request)
    {
        $data = $request->input('link');
        $res = links::insert($data);
        if ($res) {
            return redirect('admin/linksIndex');
        } else {
            $request->session()->flash('error', '添加失败！');
            return redirect('admin/linksIndex');
        }
    }



    /**
     * 编辑
     */
    public function edit($link_id)
    {
        $data = Links::where('link_id', '=', $link_id)->first();
        return view('admin.links.edit', ['data'=>$data]);
    }

    /**
     * 更改
     */
    public function update(Request $request, $link_id)
    {
        $data = $request->input('link');
        $res = Links::where('link_id', '=', $link_id)->update($data);
        if ($res) {
            return redirect('admin/linksIndex');
        } else {
            $request->session()->flash('error', '编辑失败！');
            return redirect('admin/linksIndex');
        }
    }

    /**
     * 显示
     */
    public function show($link_id)
    {
        $res = Links::where('link_id', '=', $link_id)->update(['link_status'=>1]);
        if ($res) {
            return redirect('admin/linksIndex');
        } else {
            return redirect('admin/linksIndex');
        }
    }

    /**
     * 隐藏
     */
    public function hide($link_id)
    {
        $res = Links::where('link_id', '=', $link_id)->update(['link_status'=>0]);
        if ($res) {
            return redirect('admin/linksIndex');
        } else {
            return redirect('admin/linksIndex');
        }
    }

    /**
     * 删除
     */
    public function destroy(Request $request, $link_id)
    {
        $res = Links::where('link_id',  $link_id)->delete();
        if ($res) {
            return redirect('admin/linksIndex');
        } else {
            $request->session()->flash('error', '删除失败！');
            return redirect('admin/linksIndex');
        }
    }
}
