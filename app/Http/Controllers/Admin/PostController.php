<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Model\Admin\Post;
use DB;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Validator;
use App\Model\Admin\Category;
use App\Model\Admin\TermRelationships;

class PostController extends AdminBaseController
{
    /**
     * 列表
     */
    public function index()
    {
        $tree = new \Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $terms = Category::get()->toArray();
        $new_terms=array();
        foreach ($terms as $r) {
            $r['id']=$r['term_id'];
            $r['parentid']=$r['parent'];
            $r['selected']= (!empty($parentid) && $r['term_id']==$parentid)? "selected":"";
            $new_terms[] = $r;
        }
        $tree->init($new_terms);
        $tree_tpl="<option value='\$id' \$selected>\$spacer\$name</option>";
        $taxonomys=$tree->get_tree(0,$tree_tpl);
        $posts = Post::where('post_status', '!=', 3)->paginate(15);
        return view('admin.post.index', ['posts'=>$posts, 'taxonomys'=>$taxonomys]);
    }

    /**
     * 添加
     */
    public function create(Request $request, $parentid = 0)
    {
        $tree = new \Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $terms = Category::get()->toArray();
        $new_terms=array();
        foreach ($terms as $r) {
            $r['id']=$r['term_id'];
            $r['parentid']=$r['parent'];
            $r['selected']= (!empty($parentid) && $r['term_id']==$parentid)? "selected":"";
            $new_terms[] = $r;
        }
        $tree->init($new_terms);
        $tree_tpl="<option value='\$id' \$selected>\$spacer\$name</option>";
        $taxonomys=$tree->get_tree(0,$tree_tpl);
        return view('admin.post.create', ['categorys'=>$taxonomys]);
    }

    /**
     * 保存
     */
    public function store(Request $request)
    {
        if($request->isMethod('post')){
            if(empty($request->input('term'))){
                $request->session()->flash('error_cat', '请至少选择一个分类！');
                return redirect('admin/postCreate');
            }
            $post = $request->input('post');
            if(empty($post['post_title'])){
                $request->session()->flash('error_title', '标题不能为空！');
                return redirect('admin/postCreate');
            }
            if (empty($post['post_date'])) {
                $post['post_date'] = time();
            } else {
                $post['post_date'] = strtotime($post['post_date']);
            }
            $post['post_modified'] = $post['post_date'];
            if (empty($post['post_keywords'])) {
                unset($post['post_keywords']);
            }
            $post['post_content'] = htmlspecialchars_decode($post['post_content']);
            $image['smeta']['thumb'] = $request->input('smeta')['thumb'];
            $image = json_encode($image);
            $post['smeta'] = $image;
            $res =  Post::insertGetId($post);
            if ($res) {
                foreach ($request->input('term') as $vo) {
                    TermRelationships::insert([
                        'object_id'=> $res,
                        'term_id'=> $vo,
                        'status'=> $post['post_status']
                    ]);
                }
                return redirect('admin/postIndex');
            } else {
                $request->session()->flash('error', '添加失败！');
                return redirect('admin/postCreate');
            }
        }
    }

    /**
     * 详情
     */
    public function show($term_id)
    {

    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $data = Post::where('id', $id)->first()->toArray();
        $data['smeta'] = json_decode($data['smeta'], 'array')['smeta']['thumb'] ;
        $data['post_date'] = date('Y-m-d H:i:s', $data['post_date']);
        // 文章所在的分类
        $term = TermRelationships::where('object_id', $id)->select('term_id')->get()->toArray();
        foreach ($term as $ko => $vo){
            $term[$ko] = $vo['term_id'];
        }
        $tree = new \Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $terms = Category::get()->toArray();
        $new_terms=array();
        foreach ($terms as $r) {
            $r['id']=$r['term_id'];
            $r['parentid']=$r['parent'];
            $r['selected']= (in_array($r['term_id'], $term))? "selected":"";
            $new_terms[] = $r;
        }
        $tree->init($new_terms);
        $tree_tpl="<option value='\$id' \$selected>\$spacer\$name</option>";
        $taxonomys=$tree->get_tree(0,$tree_tpl);
        return view('admin.post.edit', ['data'=>$data, 'taxonomys'=>$taxonomys]);
    }

    /**
     * 更改
     */
    public function update(Request $request, $id)
    {
        if($request->isMethod('post')){
            if(empty($request->input('term'))){
                $request->session()->flash('error_cat', '请至少选择一个分类！');
                return redirect("admin/postEdit/{$id}");
            }
            $post = $request->input('post');
            if(empty($post['post_title'])){
                $request->session()->flash('error_title', '标题不能为空！');
                return redirect("admin/postEdit/{$id}");
            }
            if (empty($post['post_date'])) {
                $post['post_date'] = time();
            } else {
                $post['post_date'] = strtotime($post['post_date']);
            }
            $post['post_modified'] = $post['post_date'];
            if (empty($post['post_keywords'])) {
                unset($post['post_keywords']);
            }
            $post['post_content'] = htmlspecialchars_decode($post['post_content']);
            $image['smeta']['thumb'] = $request->input('smeta')['thumb'];
            $post['smeta'] = json_encode($image);
            // 文章内容
            $res =  Post::where('id', $id)->update($post);
            // 文章分类
            $result = TermRelationships::where('object_id', $id)->delete();
            if ($result) {
                foreach ($request->input('term') as $vo) {
                    $res_cat = TermRelationships::insert([
                        'object_id'=> $id,
                        'term_id'=> $vo,
                        'status'=> $post['post_status']
                    ]);
                }
                if ($res_cat) {
                    $request->session()->flash('error', '编辑成功！');
                    return redirect("admin/postEdit/{$id}");
                } else {
                    $request->session()->flash('error', '编辑失败！');
                    return redirect("admin/postEdit/{$id}");
                }
            } else {
                $request->session()->flash('error', '编辑失败！');
                return redirect("admin/postEdit/{$id}");
            }
        }
    }

    /**
     * 删除
     */
    public function destroy(Request $request, $id)
    {
        $res = Post::where('id',  $id)->update(['post_status'=>3]);
        if ($res) {
            return redirect('admin/postIndex');
        } else {
            $request->session()->flash('error', '删除失败！');
            return redirect('admin/postIndex');
        }
    }

    /**
     * 图片上传
     */
    public function upload()
    {
        $upload=new \UploadFile();
        $upload->maxSize=1*1024*1024;//默认为-1，不限制上传大小
        $upload->savePath='uploads/webuploader/';//上传根目录
        $upload->saveRule='uniqid';//上传文件的文件名保存规则
        $upload->uploadReplace=true;//如果存在同名文件是否进行覆盖
        $upload->autoSub=true;//上传子目录开启
        $upload->subType='date';//上传子目录命名规则
        $upload->allowExts=array('jpg', 'jpeg', 'gif', 'bmp');// 允许类型
        if($upload->upload()){
            return $info=$upload->getUploadFileInfo();
        }else{
            return $upload->getErrorMsg();
        }
    }

    /**
     * 审核
     */
    public function status(Request $request, $id, $status)
    {
        $res = Post::where('id', $id)->update(['post_status'=>$status]);
        if ($res) {
            return redirect('admin/postIndex');
        } else {
            $request->session()->flash('error', '操作失败！');
            return redirect('admin/postIndex');
        }
    }

    /**
     * 置顶
     */
    public function istop(Request $request, $id, $istop)
    {
        $res = Post::where('id', $id)->update(['istop'=>$istop]);
        if ($res) {
            return redirect('admin/postIndex');
        } else {
            $request->session()->flash('error', '操作失败！');
            return redirect('admin/postIndex');
        }
    }

    /**
     * 推荐
     */
    public function recommended(Request $request, $id, $recommended)
    {
        $res = Post::where('id', $id)->update(['recommended'=>$recommended]);
        if ($res) {
            return redirect('admin/postIndex');
        } else {
            $request->session()->flash('error', '操作失败！');
            return redirect('admin/postIndex');
        }
    }

    /**
     * 批量审核
     */
    public function batch_status(Request $request, $id, $status)
    {
        $res = Post::where('id', $id)->update(['post_status'=>$status]);
        if ($res) {
            return redirect('admin/postIndex');
        } else {
            $request->session()->flash('error', '操作失败！');
            return redirect('admin/postIndex');
        }
    }

    /**
     * 批量置顶
     */
    public function batch_istop(Request $request, $id, $istop)
    {
        $res = Post::where('id', $id)->update(['istop'=>$istop]);
        if ($res) {
            return redirect('admin/postIndex');
        } else {
            $request->session()->flash('error', '操作失败！');
            return redirect('admin/postIndex');
        }
    }

    /**
     * 批量推荐
     */
    public function batch_recommended(Request $request, $id, $recommended)
    {
        $res = Post::where('id', $id)->update(['recommended'=>$recommended]);
        if ($res) {
            return redirect('admin/postIndex');
        } else {
            $request->session()->flash('error', '操作失败！');
            return redirect('admin/postIndex');
        }
    }

    /**
     * 批量复制
     */
    public function copy()
    {

    }

}
