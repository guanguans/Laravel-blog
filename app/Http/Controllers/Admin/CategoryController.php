<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Model\Admin\Category;
use DB;
use Validator;

class CategoryController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorys = Category::get()->toArray();
        $tree = new \Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        foreach ($categorys as $r) {
            $r['str_manage'] = '<a href="' . url("admin/categoryCreate", array("parent" => $r['term_id'])) . '">'.'添加子分类'.'</a> | 
                                <a href="' . url("admin/categoryShow", array("id" => $r['term_id'])) . '">'.'编辑'.'</a> |
                                <a class="js-aja-delete confirm-delete" data="' . url("admin/categoryDestroy", array("id" => $r['term_id'])) . '">'.'删除'.'</a> ';
            $r['id']=$r['term_id'];
            $r['parentid']=$r['parent'];
            $array[] = $r;
        }
        $tree->init($array);
        $str = "<tr>
					<td><input name='listorders[\$id]' type='text' size='1' value='\$listorder' class='input input-order'></td>
					<td>\$id</td>
					<td>\$spacer\$name</td>
	    			<td>缩略图</td>
					<td>\$str_manage</td>
				</tr>";
        $categorys = $tree->get_tree(0, $str);
        return view('admin.category.index', ['categorys'=>$categorys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $parentid = 0)
    {
        $tree = new \Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $terms = Category::get()->toArray();
        $new_terms=array();
        foreach ($terms as $r) {
            $r['id']=$r['term_id'];
            $r['parentid']=$r['parent'];
            $r['selected']= (!empty($parentid) && $r['term_id']==$parentid)? "selected":"";
            $new_terms[] = $r;
        }
        $tree->init($new_terms);
        $tree_tpl="<option value='\$id' \$selected>\$spacer\$name</option>";
        $taxonomys=$tree->get_tree(0,$tree_tpl);
        return view('admin.category.create', ['categorys'=>$taxonomys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parent = $request->input('parent');
        $name = $request->input('name');
        $description = $request->input('description');
        $id = Category::insertGetId(['name' => $name]);
        if ($parent) {
            $parentPath = Category::where('term_id', $parent)->select('path')->first()->toArray();
            $path = $parentPath['path'].'-'.$id;
        } else {
            $path = '0-'.$id;
        }
        $res = Category::where('term_id', $id)->update(['path'=> $path, 'parent'=>$parent, 'description'=>$description]);
        if ($res) {
            return redirect('admin/categoryIndex');
        } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($term_id)
    {
        $parentid = Category::where('term_id', $term_id)->select('parent')->first();
        $parentid = $parentid['parent'];
        $tree = new \Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $terms = Category::get()->toArray();
        $new_terms=array();
        foreach ($terms as $r) {
            $r['id']=$r['term_id'];
            $r['parentid']=$r['parent'];
            $r['selected']= (!empty($parentid) && $r['term_id']==$parentid)? "selected":"";
            $new_terms[] = $r;
        }
        $tree->init($new_terms);
        $tree_tpl="<option value='\$id' \$selected>\$spacer\$name</option>";
        $categorys=$tree->get_tree(0,$tree_tpl);
        $category = Category::where('term_id', $term_id)->select('term_id', 'name', 'description')->first();
        return view('admin.category.show', ['category'=>$category, 'categorys'=>$categorys]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $rule = [
            'name' => 'required',
        ];
        $message = [
            'name.required' => '名称不能为空！',
        ];
        $validate = Validator::make($input, $rule, $message);
        if ($validate->fails()) {
            return back()->withErrors($validate);
        }
        $parent = $request->input('parent');
        $name = $request->input('name');
        $description = $request->input('description');
        if ($parent) {
            $parentPath = Category::where('term_id', $parent)->select('path')->first();
            $parentPath = $parentPath['path'];
        } else {
            $parentPath = 0;
        }
        $path = $parentPath.'-'.$id;
        $res = Category::where('term_id', $id)->update([
            'name'=>$name,
            'description'=>$description,
            'path'=>$path,
        ]);
        if ($res) {
            return redirect('admin/categoryIndex');
        } else {
            $request->session()->flash('error', '编辑失败！');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $res = Category::where('parent', $id)->select('term_id')->first();
        if ($res) {
            $request->session()->flash('error', '该分类下还有子类，无法删除！');
            return back();
        }
        $delete = Category::where('term_id', $id)->delete();
        if ($delete) {
            return redirect('admin/categoryIndex');
        } else {
            $request->session()->flash('error', '删除失败！');
            return back();
        }
    }
}
