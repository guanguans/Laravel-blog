<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use DB;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Validator;

class PersonController extends AdminBaseController
{
    /**
     * 脨脜脧垄卤脿录颅
     */
    public function editInfo($user_id)
    {
        $data = DB::table('cmf_users')->where('id', '=', $user_id)->first();
        return view('admin.person.editInfo', ['data'=>$data]);
    }

    /**
     * 脨脜脧垄赂眉赂脛
     */
    public function updateInfo(Request $request, $user_id)
    {
        $data = $request->input('person');
        $res = DB::table('cmf_users')->where('id', '=', $user_id)->update($data);
        if ($res) {
            $request->session()->flash('error', '脨脼赂脛鲁脡鹿娄拢隆');
            return redirect('admin/personEditInfo/'.$user_id);
        } else {
            $request->session()->flash('error', '脨脼赂脛脢搂掳脺拢隆');
            return redirect('admin/personEditInfo/'.$user_id);
        }
    }

    /**
     * 脙脺脗毛卤脿录颅
     */
    public function editPass($user_id)
    {
        return view('admin.person.editPass');
    }

    /**
     * 脙脺脗毛赂眉赂脛
     */
    public function updatePass(Request $request, $user_id)
    {
        $old_pass = $request->input('old_pass');
        $new_pass = $request->input('new_pass');
        $re_pass = $request->input('re_pass');
        if ($re_pass != $old_pass) {
            return redirect('admin/personEditPass/'.$user_id);
            exit;
        }
        $res = DB::table('cmf_users')
            ->where('id', '=', $user_id)
            ->where('user_pass', '=', en_password($old_pass))
            ->first();
        if (!$res) {
            return redirect('admin/personEditPass/'.$user_id);
            exit;
        }
        $res = DB::table('cmf_users')
            ->where('id', '=', $user_id)
            ->update(['user_pass'=>en_password($new_pass)]);
        if ($res) {
            return redirect('admin/personEditPass/'.$user_id);
        } else {
            return redirect('admin/personEditPass/'.$user_id);
        }
    }

}
