<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Model\Admin\User;
use DB;
use Validator;

class UserController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('admin.user.index', ['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $rule = [
            'user_login' => 'required|min:5|max:8',
            'user_pass' => 'required|min:5|max:10',
            're_user_pass' => 'required|same:user_pass',
        ];
        $message = [
            'user_login.required' => '用户名不能为空',
            'user_pass.required' => '密码不能为空',
            're_user_pass.required' => '重复密码不能为空',
            're_user_pass.same' => '两次密码不一样',
        ];
        $validate = Validator::make($input, $rule, $message);
        if ($validate->fails()) {
            return back()->withErrors($validate);
        }
//        $this->validate($request, [
//            'user_login' => 'required|min:5|max:8',
//            'user_pass' => 'required|min:5|max:10',
//            're_user_pass' => 'required|same:user_pass',
//        ]);
        $user_login = $request->input('user_login');
        $user_pass = en_password($request->input('user_pass'));
        $res = DB::table('cmf_users')->where('user_login' ,$user_login)->select('id')->first();
        if ($res) {
            $request->session()->flash('user_login', '用户名已存在！');
            return back();
        }
        $res = DB::table('cmf_users')->insert([
            'user_login'=>$user_login,
            'user_pass'=>$user_pass,
        ]);
        if ($res) {
            return redirect('admin/userIndex');
        } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
