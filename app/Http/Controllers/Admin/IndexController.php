<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use DB;

class IndexController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $system_info = system_info();
        $uid = $request->Session()->get('admin_id');
        $res = DB::table('cmf_users')->where('id', $uid)->select('last_login_ip', 'last_login_time')->first();
        $last_login_ip = $res->last_login_ip;
        $last_login_time = date('Y-m-d H:i:s', $res->last_login_time);
        return view('admin.index', ['system_info'=>$system_info, 'last_login_time'=>$last_login_time, 'last_login_ip'=>$last_login_ip]);
    }

}
