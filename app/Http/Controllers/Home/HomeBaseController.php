<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class HomeBaseController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
}
