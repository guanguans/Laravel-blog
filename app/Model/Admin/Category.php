<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'cmf_terms';
    protected $primaryKey = 'term_id';
    public $timestamps = false;
}
