<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'cmf_users';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
