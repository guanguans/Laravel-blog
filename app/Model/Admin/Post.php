<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'cmf_posts';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
