<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class TermRelationships extends Model
{
    protected $table = 'cmf_term_relationships';
    protected $primaryKey = 'tid';
    public $timestamps = false;
}
