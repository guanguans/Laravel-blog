<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    protected $table = 'cmf_Links';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
