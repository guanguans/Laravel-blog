@extends('admin.base')


@section('head')
    @parent
    <link href="/admins/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>内容管理 > </h5>
                <h5>回收站 > </h5>
                <h5>文章回收</h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class="active"><a >文章回收</a></li>
                </ul>
                <div class="ibox float-e-margins">
                    <div class="ibox-content" style="background: #f5f5f5;">
                        <form role="form" class="form-inline form-search" >
                            <div class="form-group">
                                分类：
                                <select name="" id="" width="400" class="form-control">
                                    <option value="">全部</option>
                                    {!! $taxonomys !!}
                                </select>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="form-group">
                                时间：
                                <input type="text" name="start_time" class="form-control" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"> -
                                <input type="text" name="start_time" class="form-control" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="form-group">
                                关键字：
                                <input type="text"  placeholder="名称/关键字" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info" value="搜索">
                            </div>
                            <div class="form-group">
                                <a class="btn btn-danger" href="#">清空</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <div class="table-actions">
                                    <button class="btn btn-info btn-small js-ajax-submit" type="submit" data-action="/rock/index.php?g=&amp;m=AdminPost&amp;a=check&amp;check=1" data-subcheck="true">审核</button>
                                    <button class="btn btn-info btn-small js-ajax-submit" type="submit" data-action="/rock/index.php?g=&amp;m=AdminPost&amp;a=check&amp;uncheck=1" data-subcheck="true">取消审核</button>
                                    <button class="btn btn-info btn-small js-ajax-submit" type="submit" data-action="/rock/index.php?g=&amp;m=AdminPost&amp;a=top&amp;top=1" data-subcheck="true">置顶</button>
                                    <button class="btn btn-info btn-small js-ajax-submit" type="submit" data-action="/rock/index.php?g=&amp;m=AdminPost&amp;a=top&amp;untop=1" data-subcheck="true">取消置顶</button>
                                    <button class="btn btn-info btn-small js-ajax-submit" type="submit" data-action="/rock/index.php?g=&amp;m=AdminPost&amp;a=recommend&amp;recommend=1" data-subcheck="true">推荐</button>
                                    <button class="btn btn-info btn-small js-ajax-submit" type="submit" data-action="/rock/index.php?g=&amp;m=AdminPost&amp;a=recommend&amp;unrecommend=1" data-subcheck="true">取消推荐</button>
                                    <button class="btn btn-info btn-small js-articles-copy" type="button">批量复制</button>
                                    <button class="btn btn-info btn-small js-ajax-submit" type="submit" data-action="/rock/index.php?g=&amp;m=AdminPost&amp;a=delete" data-subcheck="true" data-msg="您确定删除吗？">删除</button>
                                </div>
                                <thead>
                                <tr>
                                    <th width="15"><label><input type="checkbox" class="js-check-all" data-direction="x" data-checklist="js-check-x"></label></th>
                                    <th width="50">ID</th>
                                    <th>标题</th>
                                    <th width="90">作者</th>
                                    <th width="90">点击量</th>
                                    <th width="90">评论量</th>
                                    <th width="170">关键字/来源/摘要/缩略图</th>
                                    <th width="150">发布时间</th>
                                    <th width="170">状态</th>
                                    <th width="220">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($posts as $vo)
                                    <tr>
                                        <td><input type="checkbox" class="js-check" name="ids[]" value="{{ $vo->id }}" ></td>
                                        <td><b>{{ $vo->id }}</b></td>
                                        <td>{{ $vo->post_title }}</td>
                                        <td>{{ $vo->post_author }}</td>
                                        <td>{{ $vo->post_hits }}</td>
                                        <td>评论量</td>
                                        <td>
                                            @if($vo->post_keywords)
                                                有/
                                            @else
                                                无/
                                            @endif

                                            @if($vo->post_source)
                                                有/
                                            @else
                                                无/
                                            @endif

                                            @if($vo->post_excerpt)
                                                有/
                                            @else
                                                无/
                                            @endif
                                            @php
                                                $thumb = json_decode($vo->smeta, 1)['smeta']['thumb'];
                                            @endphp
                                            @if($thumb)
                                                有
                                            @else
                                                无
                                            @endif
                                        </td>
                                        <td>@php echo date('Y-m-d H:i:s', $vo->post_date); @endphp</td>
                                        <td>
                                            @if($vo->post_status == 1)
                                                审核 |
                                            @else
                                                未审核 |
                                            @endif

                                            @if($vo->istop == 1)
                                                置顶 |
                                            @else
                                                未置顶 |
                                            @endif

                                            @if($vo->recommended == 1)
                                                推荐
                                            @else
                                                未推荐
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/recyclePostRestore', array('post_id'=>$vo['id'])) }}">还原</a> |
                                            <a class="js-aja-delete confirm-delete" data="{{ url('admin/recyclePostDestroy', array('post_id'=>$vo['id'])) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center" id="page">
                                {!! $posts->links() !!}
                            </div>
                            <style>
                                .page ul{display: inline;}
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>

    <!-- Peity -->
    <script src="/admins/js/plugins/layer/laydate/laydate.js"></script>

    <!-- Sweet alert -->
    <script src="/admins/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(function(){
            $('.confirm-delete').click(function () {
                var url = $(this).attr('data');
                swal({
                    title: "您确定要删除吗？",
                    text: "删除后将无法恢复，请谨慎操作！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "删除",
                    cancelButtonText: "取消",
                    closeOnConfirm: true
                }, function () {
                    window.location.href=url;
                });
            });
        });
        $(function(){
            var error = "{{ Session::get('error') }}";
            if (error) {
                swal({
                    title: "错误信息",
                    text: error,
                    timer: 2500 ,
                    showConfirmButton: false
                });
            }
        });
    </script>
@endsection



