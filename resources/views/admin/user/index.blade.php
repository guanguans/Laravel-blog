@extends('admin.base')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>用户管理 > </h5>
                <h5>管理组 > </h5>
                <h5><strong>管理员</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class="active"><a >管理员</a></li>
                    <li class=""><a href="{{ url('admin/userCreate') }}">管理员添加</a></li>
                </ul>
                <div class="ibox float-e-margins">
                    <div class="ibox-content" style="background: #f5f5f5;">
                        <form role="form" class="form-inline form-search" >
                            <div class="form-group">
                                用户ID：
                                <input type="text"  class="form-control">
                            </div>
                            <div class="form-group">
                                关键字：
                                <input type="text"  class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info" value="搜索">
                            </div>
                            <div class="form-group">
                                <a class="btn btn-danger" href="#">清空</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>用户名</th>
                                    <th>昵称</th>
                                    <th>头像</th>
                                    <th>邮箱</th>
                                    <th>注册时间</th>
                                    <th>最后登录时间</th>
                                    <th>最后登录IP</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $vo)
                                    <tr>
                                    <td>
                                        <input type="checkbox" checked class="i-checks" name="input[]">
                                    </td>
                                    <td>{{ $vo->id }}</td>
                                    <td>{{ $vo->user_login }}</td>
                                    <td>{{ $vo->user_nicename }}</td>
                                    <td>{{ $vo->avatar }}</td>
                                    <td>{{ $vo->user_email }}</td>
                                    <td>{{ $vo->create_time }}</td>
                                    <td>{{ $vo->last_login_time }}</td>
                                    <td>{{ $vo->last_login_ip }}</td>
                                    <td>{{ $vo->user_status }}</td>
                                    <td>
                                        <a href="table_basic.html#">拉黑</a> |
                                        <a href="table_basic.html#">启用</a>
                                    </td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <div class="text-center" id="page">
                                {!! $users->links() !!}
                            </div>
                            <style>
                                .page ul{display: inline;}
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>
@endsection



