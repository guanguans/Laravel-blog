@extends('admin.base')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>用户管理 > </h5>
                <h5>管理组 > </h5>
                <h5><strong>管理员添加</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class=""><a href="{{ url('admin/userIndex') }}">管理员</a></li>
                    <li class="active"><a>管理员添加</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <form class="form-horizontal" action="{{ url('admin/userStore') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">用户名：</label>
                                <div class="col-sm-3">
                                    <input type="text" name="user_login" placeholder="用户名" class="form-control" >
                                </div>
                                @unless (!Session::get('user_login'))
                                    <div>{{ Session::get('user_login') }}</div>
                                @endunless
                                @unless (!$errors->first('user_login'))
                                    <div>{{$errors->first('user_login')}}</div>
                                @endunless
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">密码：</label>
                                <div class="col-sm-3">
                                    <input type="password" placeholder="密码" name="user_pass" class="form-control">
                                </div>
                                @unless (!$errors->first('user_pass'))
                                    <div>{{$errors->first('user_pass')}}</div>
                                @endunless
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">重复密码：</label>
                                <div class="col-sm-3">
                                    <input type="password" placeholder="重复密码" name="re_user_pass" class="form-control">
                                </div>
                                @unless (!$errors->first('re_user_pass'))
                                    <div>{{$errors->first('re_user_pass')}}</div>
                                @endunless
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-sm btn-info" type="submit">添加</button>
                                    <a class="btn btn-sm btn-info" onclick="window.history.back();">返回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>
@endsection



