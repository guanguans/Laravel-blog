@extends('admin.base')


@section('head')
    @parent
    <link href="/admins/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/admins/css/plugins/webuploader/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/admins/css/demo/webuploader-demo.css">
    @include('UEditor::head')
    <link rel="stylesheet" type="text/css" href="/admins/js/diyUpload/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/admins/js/diyUpload/css/diyUpload.css">
    <script type="text/javascript" src="/admins/js/diyUpload/js/jquery.js"></script>
    <script type="text/javascript" src="/admins/js/diyUpload/js/webuploader.html5only.min.js"></script>
    <script type="text/javascript" src="/admins/js/diyUpload/js/diyUpload.js"></script>
@endsection

@section('content')
    <link href="/admins/css/plugins/iCheck/custom.css" rel="stylesheet">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>内容管理 > </h5>
                <h5>文章管理 > </h5>
                <h5><strong>文章编辑</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class=""><a href="{{ url('admin/postIndex') }}">文章管理</a></li>
                    <li class="active"><a>文章编辑</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">
                        <form action="{{ url('admin/postUpdate', ['id'=>$data['id']]) }}" method="post" class="form-horizontal js-ajax-forms" enctype="multipart/form-data" novalidate="novalidate" style="">
                            <div class="row-fluid" style="">
                                <span class="col-xs-9" style="">
                                    <table class="table table-bordered" style="">
                                        <tbody style=""><tr>
                                            <th width="80">分类</th>
                                            <td>
                                                <select class="form-control " width="400" required="required" multiple="multiple" style="height: 200px;width: 400px;" name="term[]">
                                                    {!! $taxonomys !!}
                                                </select>
                                                <div>windows：按住 Ctrl 按钮来选择多个选项,Mac：按住 command 按钮来选择多个选项</div>
                                                @unless (!Session::get('error_cat'))
                                                    <div style="color: red;">{{ Session::get('error_cat') }}</div>
                                                @endunless
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>标题</th>
                                            <td>
                                                <input class="form-control" type="text" style="width:400px; display: inline;"   name="post[post_title]" id="title" required="required" value="{{ $data['post_title'] }}" placeholder="请输入标题" aria-required="true">
                                                <span>*</span>
                                                @unless (!Session::get('error_title'))
                                                    <span style="color: red;">{{ Session::get('error_title') }}</span>
                                                @endunless
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>标签</th>
                                            <td>
                                                <input class="form-control" type="text" name="post[post_keywords]" id="keywords" value="{{ $data['post_keywords'] }}" style="width: 400px;display: inline;" placeholder="请输入标签">
                                                <span>多个标签之间用英文逗号隔开</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>文章来源</th>
                                            <td><input class="form-control" type="text" name="post[post_source]" id="source" value="{{ $data['post_source'] }}" style="width: 400px" placeholder="请输入文章来源"></td>
                                        </tr>
                                        <tr>
                                            <th>摘要</th>
                                            <td>
                                                <textarea class="form-control" name="post[post_excerpt]" id="description" value="" style="height: 60px;" placeholder="请填写摘要">{{ $data['post_excerpt'] }}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>内容</th>
                                            <td class="col-xs-11">
                                                <!-- 加载编辑器的容器 -->
                                                <script id="container" name="post[post_content]" type="text/plain">{!! $data['post_content'] !!}</script>
                                            </td>
                                        </tr>
                                        {{--<tr>--}}
                                            {{--<th>相册图集</th>--}}
                                            {{--<td></td>--}}
                                        {{--</tr>--}}
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-3">
                                    <table class="table table-bordered">
                                        <tbody><tr>
                                            <th><b>缩略图</b></th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="box" style="text-align: center;">
                                                    <input class="form-control" type="hidden" name="smeta[thumb]" id="thumb" value="{{ $data['smeta'] }}">
                                                    <div id="upload_image"></div>
                                                </div>
                                                @if (!is_null($data['smeta']))
                                                    <div style="text-align: center;">
                                                        <img src="/{{ $data['smeta'] }}" id="thumb-preview" width="135" style="cursor: hand" width="170" height="150"/>
                                                    </div>
                                                @endif
                                                <script>
                                                    $('#upload_image').diyUpload({
                                                        url: "{{ url('admin/postUpload') }}",
                                                        success:function( data ) {
                                                            var path = data[0].savepath + data[0].savename;
                                                            $('#thumb').val(path);
                                                            $('#thumb-preview').remove();
                                                        },
                                                        error:function( err ) {
                                                            alert(err);
//                                                            alert('上传失败！请重新上传');
                                                        },
                                                        buttonText : '选择图片',
                                                        fileNumLimit: 1
                                                    });
                                                </script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><b>发布时间</b></th>
                                        </tr>
                                        <tr>
                                            <td><input class="form-control" type="text" name="post[post_date]" value="{{ $data['post_date'] }}" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></td>
                                        </tr>
                                        <tr>
                                            <th><b>状态</b></th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="radio i-checks">
                                                    <label class="">
                                                        <div class="iradio_square-green" style="position: relative;"><input type="radio" @php echo ($data['post_status']==1? 'checked':''); @endphp value="1" name="post[post_status]" style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i></i> 审核通过
                                                    </label>
                                                </div>
                                                <div class="radio i-checks">
                                                    <label class="">
                                                        <div class="iradio_square-green" style="position: relative;"><input type="radio" @php echo ($data['post_status']==0? 'checked':''); @endphp value="0" name="post[post_status]" style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i></i> 待审核
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="radio i-checks">
                                                    <label class="">
                                                        <div class="iradio_square-green" style="position: relative;"><input type="radio" @php echo ($data['post_status']==1? 'checked':''); @endphp value="1" name="post[istop]" style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i></i> 置顶
                                                    </label>
                                                </div>
                                                <div class="radio i-checks">
                                                    <label class="">
                                                        <div class="iradio_square-green" style="position: relative;"><input type="radio" @php echo ($data['post_status']==0? 'checked':''); @endphp value="0" name="post[istop]" style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i></i> 未置顶
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="radio i-checks">
                                                    <label class="">
                                                        <div class="iradio_square-green" style="position: relative;"><input type="radio" @php echo ($data['post_status']==1? 'checked':''); @endphp value="1" name="post[recommended]" style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i></i> 推荐
                                                    </label>
                                                </div>
                                                <div class="radio i-checks">
                                                    <label class="">
                                                        <div class="iradio_square-green" style="position: relative;"><input type="radio" @php echo ($data['post_status']==0? 'checked':''); @endphp checked value="0" name="post[recommended]" style="position: absolute; opacity: 0;">
                                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                                        </div>
                                                        <i></i> 未推荐
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-xs-9 text-center">
                                <button class="btn btn-info js-ajax-submit" type="submit">提交</button>
                                <a class="btn btn-info" href="#">返回</a>
                            </div>
                            <div style="clear: both;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/layer/laydate/laydate.js"></script>
    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>

    <!-- Sweet alert -->
    <script src="/admins/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(function(){
            $('.confirm-delete').click(function () {
                var url = $(this).attr('data');
                swal({
                    title: "您确定要删除吗？",
                    text: "删除后将无法恢复，请谨慎操作！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "删除",
                    cancelButtonText: "取消",
                    closeOnConfirm: true
                }, function () {
                    window.location.href=url;
                });
            });
        });

        $(function(){
            var error = "{{ Session::get('error') }}";
            if (error) {
                swal({
                    title: "错误信息",
                    text: error,
                    timer: 2500 ,
                    showConfirmButton: false
                });
            }
        });
    </script>

    <script>
        <!-- iCheck -->
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
        <!-- 实例化编辑器 -->
        var ue = UE.getEditor('container');
        ue.ready(function() {
            ue.execCommand('serverparam', '_token', '{{ csrf_token() }}');//此处为支持laravel5 csrf ,根据实际情况修改,目的就是设置 _token 值.
        });

        $(function(){
            $("#upload_one_image").click( function () {
                $.ajax({
                    type: "POST",
                    url: "admin/postUpload",
                    data: "name=John&location=Boston",
                    success: function(msg){
                        alert( "Data Saved: " + msg );
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        // 添加全局站点信息
        var BASE_URL = 'js/plugins/webuploader';
    </script>
    <script src="/admins/js/plugins/webuploader/webuploader.min.js"></script>
    <script src="/admins/js/demo/webuploader-demo.js"></script>
@endsection



