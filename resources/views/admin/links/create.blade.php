@extends('admin.base')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>其他管理 > </h5>
                <h5>友情链接 > </h5>
                <h5><strong>友情链接添加</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class=""><a href="{{ url('admin/linksIndex') }}">友情链接</a></li>
                    <li class="active"><a>友情链接添加</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <form class="form-horizontal" action="{{ url('admin/linksStore') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">链接名称：</label>
                                <div class="col-sm-3">
                                    <input type="text" name="link[link_name]" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">链接地址：</label>
                                <div class="col-sm-3">
                                    <input type="text" name="link[link_url]" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">链接图标：</label>
                                <div class="col-sm-3">
                                    <input type="text" name="link_image" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">打开方式：</label>
                                <div class="col-sm-3">
                                    <select name="link[link_target]">
                                        <option value="_bank">新标签页打开</option>
                                        <option value="_self">本窗口打开</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">描述：</label>
                                <div class="col-sm-3">
                                    <textarea name="link[link_description]" id="" rows="5" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-sm btn-info" type="submit">添加</button>
                                    <a class="btn btn-sm btn-info" onclick="window.history.back();">返回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>
@endsection



