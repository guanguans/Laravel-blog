@extends('admin.base')


@section('head')
    @parent
    <link href="/admins/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>其他管理 > </h5>
                <h5>友情链接</h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class="active"><a >友情链接</a></li>
                    <li class=""><a href="{{ url('admin/linksCreate') }}">友情链接添加</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <div class="table-actions">
                                    <button type="submit" class="btn btn-primary btn-small js-ajax-submit">排序</button>
                                    <button type="submit" class="btn btn-primary btn-small js-ajax-submit">显示</button>
                                    <button type="submit" class="btn btn-primary btn-small js-ajax-submit">隐藏</button>
                                </div>
                                <thead>
                                <tr>
                                    <th width="30">排序</th>
                                    <th>ID</th>
                                    <th>链接名称</th>
                                    <th>链接地址</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($links as $vo)
                                <tr>
                                    <td><input type="checkbox" class="js-check" name="ids[]" value="{{ $vo->link_id }}" ></td>
                                    <td>{{ $vo->link_id }}</td>
                                    <td>{{ $vo->link_name }}</td>
                                    <td><a href="{{ $vo->link_url }}" target="_bank">{{ $vo->link_url }}</a></td>
                                    @if($vo->link_status == 1)
                                    <td>显示</td>
                                    @else()
                                    <td>隐藏</td>
                                    @endif
                                    <td>
                                        @if($vo->link_status == 1)
                                        <a href="{{ url('admin/linksHide', array('link_id'=>$vo['link_id'])) }}">隐藏</a> |
                                        @else()
                                        <a href="{{ url('admin/linksShow', array('link_id'=>$vo['link_id'])) }}">显示</a> |
                                        @endif
                                        <a href="{{ url('admin/linksEdit', array('link_id'=>$vo['link_id'])) }}">编辑</a> |
                                        <a class="js-aja-delete confirm-delete" data="{{ url('admin/linksDestroy', array('link_id'=>$vo['link_id'])) }}">删除</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent

    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>

    <!-- Sweet alert -->
    <script src="/admins/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(function(){
            $('.confirm-delete').click(function () {
                var url = $(this).attr('data');
                swal({
                    title: "您确定要删除吗？",
                    text: "删除后将无法恢复，请谨慎操作！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "删除",
                    cancelButtonText: "取消",
                    closeOnConfirm: true
                }, function () {
                    window.location.href=url;
                });
            });
        });

        $(function(){
            var error = "{{ Session::get('error') }}";
            if (error) {
                swal({
                    title: "错误信息",
                    text: error,
                    timer: 2500 ,
                    showConfirmButton: false
                });
            }
        });
    </script>
@endsection



