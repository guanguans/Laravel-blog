@extends('admin.base')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5><strong>主页</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <!-- Panel Style -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>欢迎信息</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row row-lg">
                            <div class="col-sm-12">
                                <!-- Example Cellstyle -->
                                <div class="example-wrap">
                                    <div class="example">
                                        <table data-toggle="table" data-url="js/demo/bootstrap_table_test2.json" data-mobile-responsive="true">
                                            <thead>
                                            <tr>
                                                <th data-field="name" data-cell-style="cellStyle"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td data-field="description">您好：{{ Session::get('admin_login') }}！您上一次登录时间：{{ $last_login_time }}，登录IP：{{ $last_login_ip }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- End Example Cellstyle -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>系统信息</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row row-lg">
                            <div class="col-sm-12">
                                <!-- Example Cellstyle -->
                                <div class="example-wrap">
                                    <div class="example">
                                        <table data-toggle="table" data-url="js/demo/bootstrap_table_test2.json" data-mobile-responsive="true">
                                            <thead>
                                            <tr>
                                                <th data-field="name" data-cell-style="cellStyle"></th>
                                                <th data-field="description"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($system_info as $k=>$v)
                                                <tr>
                                                    <td data-field="description">{{ $k }}</td>
                                                    <td data-field="description">{{ $v }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- End Example Cellstyle -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Panel Style -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>开发团队</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row row-lg">
                            <div class="col-sm-12">
                                <div class="example-wrap">
                                    <div class="example">
                                        <table data-toggle="table" data-url="js/demo/bootstrap_table_test2.json" data-mobile-responsive="true">
                                            <thead>
                                            <tr>
                                                <th data-field="name" data-cell-style="cellStyle"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td data-field="description">开发者：琯琯<798314049@qq.com></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Bootstrap table -->
    <script src="/admins/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="/admins/js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
    <script src="/admins/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
    <!-- Peity -->
    <script src="/admins/js/demo/bootstrap-table-demo.js"></script>
@endsection



