<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <title> - 登录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="/admins/css/bootstrap.min.css" rel="stylesheet">
    <link href="/admins/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="/admins/css/animate.css" rel="stylesheet">
    <link href="/admins/css/style.css" rel="stylesheet">
    <link href="/admins/css/login.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>
        if (window.top !== window.self) {
            window.top.location = window.location;
        }
    </script>

</head>

<body class="signin">
    <div class="signinpanel" style="text-align: center;">
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="{{ url('admin/doLogin') }}">
                    <h1 class="no-margins">琯琯后台登录</h1>
                    {!! csrf_field() !!}
                    <input type="text" class="form-control uname" placeholder="用户名" name="user_login" style="height: 40px;"/>
                    @unless (!$errors->first('user_login'))
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{$errors->first('user_login')}}
                        </div>
                    @endunless
                    <input type="password" class="form-control pword m-b" placeholder="密码" name="user_pass" style="height: 40px;"/>
                    @unless (!$errors->first('user_pass'))
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{$errors->first('user_pass')}}
                        </div>
                    @endunless
                    {{--<a href="">忘记密码了？</a>--}}
                    <img src="{{ captcha_src() }}" alt="" onclick='this.src=this.src+"?c="+Math.random()'>
                    <input type="text" class="form-control" placeholder="验证码" name="captcha" style="height: 40px;"/>
                    @unless (!$errors->first('captcha'))
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{$errors->first('captcha')}}
                        </div>
                    @endunless
                    <button class="btn btn-success btn-block" style="height: 40px;">登录</button>
                    @unless (!session('info'))
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            {{ session('info') }}
                        </div>
                    @endunless
                </form>
            </div>
        </div>
        <div class="signup-footer">
            <div class="pull-center" >
                &copy; 琯琯<798314049@qq.com>
            </div>
        </div>
    </div>
</body>
<script>

</script>
</html>
