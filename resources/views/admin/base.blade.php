<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">

    <title>琯琯后台管理</title>

    <meta name="keywords" content="">
    <meta name="description" content="">

    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    @section('head')
        <link rel="shortcut icon" href="favicon.ico">
        <link href="/admins/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
        <link href="/admins/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
        <link href="/admins/css/animate.css" rel="stylesheet">
        <link href="/admins/css/style.css?v=4.1.0" rel="stylesheet">
    @show
    <script>
        window.hdjs={};
        //组件目录必须绝对路径(在网站根目录时不用设置)
        window.hdjs.base = '/node_modules/hdjs  ';
        //上传文件后台地址
        window.hdjs.uploader = 'test/php/uploader.php?';
        //获取文件列表的后台地址
        window.hdjs.filesLists = 'test/php/filesLists.php?';
    </script>
    <script src="/node_modules/hdjs/static/requirejs/require.js"></script>
    <script src="/node_modules/hdjs/static/requirejs/config.js"></script>
</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="{{ url('admin') }}">
                                <span class="clear">
                                    <span class="block m-t-xs" style="font-size:20px;">
                                        <i class="fa fa-user"></i>
                                        <strong class="font-bold">琯琯后台管理</strong>
                                    </span>
                                </span>
                            </a>
                        </div>
                        <div class="logo-element">琯琯
                        </div>
                    </li>
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span class="ng-scope"></span>
                    </li>
                    <li class="line dk"></li>
                    <li>
                        <a href="#">
                            <i class="fa fa-cogs normal"></i>
                            <span class="nav-label">网站设置</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a class="J_menuItem" href="{{ url('admin/personEditInfo', array('user_id'=>Session::get('admin_id'))) }}">个人信息</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="#">网站信息</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="#">邮箱配置</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="#">清楚缓存</a>
                            </li>
                        </ul>
                    </li>
                    <li class="line dk"></li>
                    <li>
                        <a href="#">
                            <i class="fa fa fa-users"></i>
                            <span class="nav-label">用户管理</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">
                                    <span class="nav-label">用户组</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a class="J_menuItem" href="#">本站用户</a>
                                    </li>
                                    <li>
                                        <a class="J_menuItem" href="#">第三方用户</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="nav-label">管理组</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a class="J_menuItem" href="#">角色管理</a>
                                    </li>
                                    <li>
                                        <a class="J_menuItem" href="{{ url('admin/userIndex') }}">管理员</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="line dk"></li>
                    <li>
                        <a href="#">
                            <i class="fa fa fa-th normal"></i>
                            <span class="nav-label">内容管理</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a class="J_menuItem" href="{{ url('admin/categoryIndex') }}">分类管理</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="{{ url('admin/postIndex') }}">文章管理</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="#">留言管理</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="#">评论管理</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="nav-label">回收站</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a class="J_menuItem" href="{{ url('admin/recyclePostIndex') }}">文章回收</a>
                                    </li>
                                    <li>
                                        <a class="J_menuItem" href="#">留言回收</a>
                                    </li>
                                    <li>
                                        <a class="J_menuItem" href="#">评论回收</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="line dk"></li>
                    <li>
                        <a href="#">
                            <i class="fa fa fa-cloud normal"></i>
                            <span class="nav-label">其他管理</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a class="J_menuItem" href="#">幻灯片</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="#">广告</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="{{ url('admin/linksIndex') }}">友情链接</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="#">第三方登录</a>
                            </li>
                        </ul>
                    </li>
                    <li class="line dk"></li>
                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header" style="width: 80%;">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i></a>
                        <a class="minimalize-styl-2 btn btn-info " href="/" target="_blank"><i class="fa fa-home"></i><strong> 网站首页</strong></a>
                        <a class="minimalize-styl-2 btn btn-info " href="{{ url('admin') }}"><i class="fa fa-cog"></i><strong> 系统信息</strong></a>
                        <a class="minimalize-styl-2 btn btn-info " href="#"><i class="fa fa-align-justify"></i><strong> 分类管理</strong></a>
                        <a class="minimalize-styl-2 btn btn-info " href="#"><i class="fa fa-file-text"></i><strong> 文章管理</strong></a>
                        <a class="minimalize-styl-2 btn btn-info " href="#"><i class="fa fa-comments"></i><strong> 评论管理</strong></a>
                        <a class="minimalize-styl-2 btn btn-warning " href="#"><i class="fa fa-spinner"></i><strong> 清除缓冲</strong></a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                欢迎：<i class="fa fa-user"></i>
                                <button class="btn-info">{{ Session::get('admin_login') }}</button>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="{{ url('admin/personEditInfo', array('user_id'=>Session::get('admin_id'))) }}">
                                        <div class="text-center">
                                            <i class="fa fa-user"></i> <strong>修改信息</strong>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ url('admin/personEditPass', array('user_id'=>Session::get('admin_id'))) }}">
                                        <div class="text-center">
                                            <i class="fa fa-lock"></i> <strong>修改密码</strong>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ url('admin/logout') }}">
                                        <div class="text-center">
                                            <i class="fa fa-sign-out"></i> <strong>退出</strong>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row J_mainContent" style="background: #fff;">
                @yield('content')
                <div class="text-center" style="height: 60px;">
                    &copy;琯琯<798314049@qq.com>
                </div>
            </div>
        </div>
        <!--右侧部分结束-->
    </div>
    @section('js')
        <!-- 全局js -->
        <script src="/admins/js/jquery.min.js?v=2.1.4"></script>
        <script src="/admins/js/bootstrap.min.js?v=3.3.6"></script>
        <script src="/admins/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="/admins/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="/admins/js/plugins/layer/layer.min.js"></script>

        <!-- 自定义js -->
        <script src="/admins/js/hAdmin.js?v=4.1.0"></script>
        {{--<script src="/admins/js/index.js"></script>--}}

        <!-- 第三方插件 -->
        <script src="/admins/js/plugins/pace/pace.min.js"></script>
    @show
</body>

</html>
