@extends('admin.base')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>网站设置 > </h5>
                <h5>个人信息 > </h5>
                <h5><strong>修改信息</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class=""><a href="{{ url('admin/personEditInfo', array('user_id'=>Session::get('admin_id'))) }}">修改信息</a></li>
                    <li class="active"><a>修改密码</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <form class="form-horizontal" action="{{ url('admin/personUpdatePass', array('user_id'=>Session::get('admin_id'))) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">*原密码：</label>
                                <div class="col-sm-3">
                                    <input type="password" name="old_pass"class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">*新密码：</label>
                                <div class="col-sm-3">
                                    <input type="password" name="new_pass"class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">*重复密码：</label>
                                <div class="col-sm-3">
                                    <input type="password" name="re_pass"class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-sm btn-info" type="submit">保存</button>
                                    <a class="btn btn-sm btn-info" onclick="window.history.back();">返回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>
@endsection



