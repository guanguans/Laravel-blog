@extends('admin.base')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>网站设置 > </h5>
                <h5>个人信息 > </h5>
                <h5><strong>修改信息</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class="active"><a>修改信息</a></li>
                    <li class=""><a href="{{ url('admin/personEditPass', array('user_id'=>$data->id)) }}">修改密码</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <form class="form-horizontal" action="{{ url('admin/personUpdateInfo', array('user_id'=>$data->id)) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">*昵称：</label>
                                <div class="col-sm-3">
                                    <input type="text" name="person[user_nicename]" value="{{ $data->user_nicename }}" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">性别：</label>
                                <div class="col-sm-3">
                                    <select name="person[sex]" class="col-sm-12">
                                        @if($data->sex == 0)
                                        <option value="0" selected>保密</option>
                                        <option value="1">男</option>
                                        <option value="2">女</option>
                                        @elseif($data->sex == 1)
                                        <option value="0">保密</option>
                                        <option value="1" selected>男</option>
                                        <option value="2">女</option>
                                        @else
                                        <option value="0">保密</option>
                                        <option value="1">男</option>
                                        <option value="2" selected>女</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">*生日：</label>
                                <div class="col-sm-3">
                                    <input type="text" name="person[birthday]" value="{{ $data->birthday }}" class="form-control" required onClick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">个人网址：</label>
                                <div class="col-sm-3">
                                    <input type="url" name="person[user_url]" value="{{ $data->user_url }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">个性签名：</label>
                                <div class="col-sm-3">
                                    <textarea name="person[signature]" id="" rows="2" class="form-control">{{ $data->signature }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-sm btn-info" type="submit">保存</button>
                                    <a class="btn btn-sm btn-info" onclick="window.history.back();">返回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/layer/laydate/laydate.js"></script>

    <!-- Sweet alert -->
    <script src="/admins/js/plugins/sweetalert/sweetalert.min.js"></script>

    <script>
        $(function(){
            var error = "{{ Session::get('error') }}";
            if (error) {
                swal({
                    title: "信息",
                    text: error,
                    timer: 2500 ,
                    showConfirmButton: false
                });
            }
        });
    </script>
@endsection



