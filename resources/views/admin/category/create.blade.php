@extends('admin.base')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>内容管理 > </h5>
                <h5>分类管理 > </h5>
                <h5><strong>分类添加</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class=""><a href="{{ url('admin/categoryIndex') }}">分类管理</a></li>
                    <li class="active"><a>分类添加</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <form class="form-horizontal" action="{{ url('admin/categoryStore') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">上级：</label>
                                <div class="col-sm-3">
                                    <select name="parent"  class="form-control">
                                        <option value="0">作为一级分类</option>
                                        {!! $categorys !!}
                                    </select>
                                </div>
                                @unless (!Session::get('user_login'))
                                    <div>{{ Session::get('user_login') }}</div>
                                @endunless
                                @unless (!$errors->first('user_login'))
                                    <div>{{$errors->first('user_login')}}</div>
                                @endunless
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">分类名称：</label>
                                <div class="col-sm-3">
                                    <input type="text" placeholder="分类名称" name="name" class="form-control">
                                </div>
                                @unless (!$errors->first('user_pass'))
                                    <div>{{$errors->first('user_pass')}}</div>
                                @endunless
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">描述：</label>
                                <div class="col-sm-3">
                                    <textarea name="description" id="" rows="5" class="form-control"></textarea>
                                </div>
                                @unless (!$errors->first('re_user_pass'))
                                    <div>{{$errors->first('re_user_pass')}}</div>
                                @endunless
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-sm btn-info" type="submit">添加</button>
                                    <a class="btn btn-sm btn-info" onclick="window.history.back();">返回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>
@endsection



