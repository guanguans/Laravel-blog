@extends('admin.base')


@section('head')
    @parent
    <link href="/admins/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>内容管理 > </h5>
                <h5>分类管理</h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class="active"><a >分类管理</a></li>
                    <li class=""><a href="{{ url('admin/categoryCreate') }}">分类添加</a></li>
                </ul>
                <div class="ibox float-e-margins">
                    <div class="ibox-content" style="background: #f5f5f5;">
                        <form role="form" class="form-inline form-search" >
                            <div class="form-group">
                                关键字：
                                <input type="text"  class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info" value="搜索">
                            </div>
                            <div class="form-group">
                                <a class="btn btn-danger" href="#">清空</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <div class="table-actions">
                                    <button type="submit" class="btn btn-primary btn-small js-ajax-submit">排序</button>
                                </div>
                                <thead>
                                <tr>
                                    <th width="30">排序</th>
                                    <th>ID</th>
                                    <th>分类名称</th>
                                    <th>缩略图</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                {!! $categorys !!}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent

    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>

    <!-- Sweet alert -->
    <script src="/admins/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(function(){
            $('.confirm-delete').click(function () {
                var url = $(this).attr('data');
                swal({
                    title: "您确定要删除吗？",
                    text: "删除后将无法恢复，请谨慎操作！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "删除",
                    cancelButtonText: "取消",
                    closeOnConfirm: true
                }, function () {
                    window.location.href=url;
                });
            });
        });

        $(function(){
            var error = "{{ Session::get('error') }}";
            if (error) {
                swal({
                    title: "错误信息",
                    text: error,
                    timer: 2500 ,
                    showConfirmButton: false
                });
            }
        });
    </script>
@endsection



