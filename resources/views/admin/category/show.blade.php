@extends('admin.base')


@section('head')
    @parent
    <link href="/admins/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-12">
            <div class="ibox-title">
                <h5>内容管理 > </h5>
                <h5>分类管理 > </h5>
                <h5><strong>分类编辑</strong></h5>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 tabs-container">
                <ul class="nav nav-tabs" style="margin-bottom: 20px;">
                    <li class=""><a href="{{ url('admin/categoryIndex') }}">分类管理</a></li>
                    <li class="active"><a>分类编辑</a></li>
                </ul>
                <!-- Panel Style -->
                <div class="ibox float-e-margins wrap">
                    <div class="ibox-content">
                        <form class="form-horizontal" action="{{ url('admin/categoryUpdate', ['id'=>$category->term_id]) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">上级：</label>
                                <div class="col-sm-3">
                                    <select name="parent"  class="form-control">
                                        <option value="0">作为一级分类</option>
                                        {!! $categorys !!}
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">分类名称：</label>
                                <div class="col-sm-3">
                                    <input type="text" value="{{ $category->name }}" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">描述：</label>
                                <div class="col-sm-3">
                                    <textarea name="description" id="" rows="5" class="form-control">{{ $category->description }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-sm btn-info" type="submit">添加</button>
                                    <a class="btn btn-sm btn-info" onclick="window.history.back();">返回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="/admins/js/content.js"></script>
    <!-- Peity -->
    <script src="/admins/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- 自定义js -->
    <script src="/admins/js/content.js?v=1.0.0"></script>


    <!-- iCheck -->
    <script src="/admins/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Peity -->
    <script src="/admins/js/demo/peity-demo.js"></script>

    <!-- Sweet alert -->
    <script src="/admins/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(function(){
            var error = "{{ $errors->first('name') }}";
            if (error) {
                swal({
                    title: "错误信息",
                    text: error,
                    timer: 2000 ,
                    showConfirmButton: false
                });
            }
        });
        $(function(){
            var error = "{{ Session::get('error') }}";
            if (error) {
                swal({
                    title: "错误信息",
                    text: error,
                    timer: 2000 ,
                    showConfirmButton: false
                });
            }
        });
    </script>
@endsection



